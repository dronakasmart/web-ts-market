import { Ivehicle } from './ivehicle';

export class Motorcycle implements Ivehicle {

    ride () {}

    stop () {}

    color: string;
    weight: number;

    x_pos: number;
    y_pos: number;
    is_stoped: boolean;
    fuel: number;
    name: string;
}