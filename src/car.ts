import {Ivehicle} from "./ivehicle";

export class Car implements Ivehicle {
    ride() {
    }

    stop() {
    }

    color: string;
    speed: number;
    weight: number;

    x_pos: number;
    y_pos: number;
    is_stoped: boolean;
    fuel: number;
    name: string;
}
