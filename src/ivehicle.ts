export interface Ivehicle {
    ride();
    stop();

    x_pos: number;
    y_pos: number;
    is_stoped: boolean;
    fuel: number;
    name: string;
}