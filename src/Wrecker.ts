import { Ivehicle } from "./ivehicle";

export class Wrecker implements Ivehicle {

    is_stoped: boolean;
    x_pos: number;
    y_pos: number;
    fuel: number;
    name: string;
    x_pos_ts: number;
    y_pos_ts: number;
    name_ts: string;

    constructor (name_ts, x, y) {
        this.x_pos_ts = x;
        this.y_pos_ts = y;
        this.name_ts = name_ts;
    }

    ride () {
        let timer = setInterval(() => {
            if(this.x_pos != this.x_pos_ts && this.y_pos != this.y_pos_ts) {
                this.x_pos++;
                this.y_pos++;
            } else {
                this.stop();
                clearInterval(timer);
            }
        }, 100);
    }

    stop () {
        this.is_stoped = true;
        console.log(this.name + ' arrived at ' + this.name_ts);
    }
}