import { Bmw } from "./bmw";
import { Ivehicle } from "./ivehicle";
import { Draw } from './Draw';
import { Yamaha } from './Yamaha';
import { Wrecker } from './Wrecker';

export class Road {
    vehicles: Ivehicle[] = [];

    constructor(){
        this.add(this.createBmw());
        this.add(this.createYamaha());
        this.start();
    }

    start(){
        let draw = new Draw();
        setInterval(() => {
            this.vehicles.forEach((item) => {
                if(item.is_stoped) {
                    item.stop();
                    let wrecker = this.createWrecker(item.name, item.x_pos, item.y_pos);
                    wrecker.ride();
                    this.vehicles.pop();
                } else {
                    item.ride();
                    draw.DrawVehicle(item.name, item.x_pos, item.y_pos);        
                }                
            });
        }, 250);
    }

    add(t: Ivehicle){
        this.vehicles.push(t);
    }

    createBmw () {
        let bmw = new Bmw();
        bmw.name = 'Bmw';
        bmw.color = '#ff0000';
        bmw.speed = 90;
        bmw.weight = 1000;
        bmw.fuel = 100;
        bmw.x_pos = 0;
        bmw.y_pos = 0;

        return bmw;
    }

    createYamaha () {
        let yamaha = new Yamaha();
        yamaha.name = 'Yamaha';
        yamaha.color = 'ff3333';
        yamaha.weight = 150;
        yamaha.fuel = 50;
        yamaha.x_pos = 0;
        yamaha.y_pos = 0;

        return yamaha;
    }

    createWrecker (name_ts, x, y) {
        let wrecker = new Wrecker(name_ts, x, y);
        wrecker.name = 'Wrecker';
        wrecker.is_stoped = false;
        wrecker.x_pos = 0;
        wrecker.y_pos = 0;

        return wrecker;
    }
}
