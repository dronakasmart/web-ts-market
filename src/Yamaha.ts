import { Motorcycle } from './Motorcycle';

export class Yamaha extends Motorcycle {

    ride () {
        this.x_pos += 1;
        this.y_pos += 1;
        this.fuel != 0 ? this.fuel -= 1 : this.is_stoped = true;
    }

    stop () {
        console.log('---', this.name + ' is stoped');
        console.log('position x: ' + this.x_pos);
        console.log('position y: ' + this.y_pos);
    }
}